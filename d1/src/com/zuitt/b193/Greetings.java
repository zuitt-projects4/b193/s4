package com.zuitt.b193;

public interface Greetings {

    public void morningGreet();
    public void holidayGreet();
}
