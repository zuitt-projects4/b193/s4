package com.zuitt.b193;

public class Dog extends Animal {

    private String breed;

    public Dog(){
        //super() will reference the variable from the Animal Class.
        //Animal() Constructor by using , we can use the instance variable, we can invoke immediate parent class constructor and class method.
        super();
        this.breed = "chihuahua";
    }
    public Dog(String name, String color, String breed){
        super(name, color);//Animal(String name, String color);
        this.breed = breed;
    }

    public String getBreed() {
        return this.breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public void speak(){
        super.call();//the call method of the Animal class.
        System.out.println("Bark");
    }
}
