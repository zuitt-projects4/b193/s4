package com.zuitt.b193;

public class Driver {

    private String name;

    public Driver(){};

    public Driver(String name) {
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public String setName(){
        return this.name;
    }

}
