package com.zuitt.b193;

public interface Actions {

    //interface are used to achieve abstraction, users of our app will be able to use the methods but not be able to see how those methods work.

    //Classes cannot have a multiple inheritance.
    //Classes can implement the multiple interfaces.
    // you can think of interfaces as class for classes or blueprints for blueprint.
    public void sleep();

    public void run();



}
