package com.zuitt.b193;

public class Car {

    //blueprint of our Car Object
    //class creation - is composed of four parts:
    //1. Properties - characteristics of the Objects
    //2. Constructors - used to create an object
    //3. Getters/Setters - get and set the values of each property of the object.
    //4. Methods - functions of an object where it can perform a task

    //Access modifiers
    //1. Default - no keyword required. Only those classes that are in the same package can access this class. No other class outside the package can access the class.
    //2. Private - properties and methods are only accessible within the class.
    //3. Public - the members, methods, and classes that are declared public can be accessed from anywhere.
    //4. Protected -


    //Properties = qualities or characteristic of real world objects()
    public String name;
    private String brand;
    private int manufactureDate;
    private String owner;

    //make Driver component
    private Driver d;
    private Passenger p;


    //constructors
    //empty constructors- it is a common practices to create an empty and parameterized constructors for creating new instances

    public Car(){
        //add a driver so whenever a new car is created, it will always have a driver.
        this.d = new Driver("Alejandro");
        this.p = new Passenger("Lady Gaga");//whenever a new car is created, it will always has a passenger "LadyGaga"
    };

    //parameterized constructor = accepts properties parameter
    public Car(String name, String brand, int manufactureDate, String owner){
        this.name = name;
        this.brand = brand;
        this.manufactureDate = manufactureDate;
        this.owner = owner;
    }

    //Getter and Setters = this is used for retrieving and changing the properties(write-only, read-only)

    public String getDriverName(){
        return this.d.getName();
    }

    public String getPassengerName(){
        return this.p.getPassengerName();
    }
    //getters - read-only or just getting the properties


    public String getName(){
        return this.name;
    }

    public String getBrand(){

        return this.brand;
    }

    public int getManufactureDate(){

        return this.manufactureDate;
    }

    public String getOwner(){

        return this.owner;
    }

    //setters  - write-only or for setting up the properties
    public void setName(String name){
        this.name = name;
    }

    public void  setBrand(String brand){
        this.brand = brand;
    }

    public void setManufactureDate(int manufactureDate){
        this.manufactureDate = manufactureDate;
    }

    public void setOwner(String owner){
        this.owner = owner;
    }

    //methods
    public void drive(){
        System.out.println(getOwner() + " drives the " + getBrand());
    }

    public void printDetails(){
        System.out.println("This " + getManufactureDate() + " " + getBrand() + " " + getName() + " is owned by " + getOwner() );
    }




}
