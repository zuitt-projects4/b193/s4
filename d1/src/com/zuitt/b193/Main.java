package com.zuitt.b193;

public class Main {

    public static void main(String[] args){

        //instantiation of objects
        //new instances with an empty constructor
        Car myFirstCar = new Car();
        myFirstCar.setName("Civic");
        myFirstCar.setBrand("Honda");
        myFirstCar.setManufactureDate(1998);
        myFirstCar.setOwner("John");
        System.out.println(myFirstCar.getName());



        // new instances with parameterized parameter
        Car mySecondCar = new Car("Charger", "Dodge" , 1978, "Vin Diesel");

        //we used the getters from the Car
        System.out.println(mySecondCar.getName());
        System.out.println(mySecondCar.getBrand());
        System.out.println(mySecondCar.getManufactureDate());
        System.out.println(mySecondCar.getOwner());

        //methods
        myFirstCar.drive();
        mySecondCar.drive();

        myFirstCar.printDetails();
        mySecondCar.printDetails();

        //encapsulation - we bundled each field and methods inside a single class and we used access modifiers like public to allow the access from another class.

        // why we used encapulation
        //the fields of a class can be made read-only and write-only a class can have 
        Car anotherCar = new Car();
        anotherCar.setName("Ferrari");
        System.out.println(anotherCar.getName());

        anotherCar.name = "Honda";
        System.out.println(anotherCar.getName());

        Car newCar = new Car();
        System.out.println("this car is driven by " + newCar.getDriverName());

        System.out.println("This car has a passenger name " + newCar.getPassengerName());

        //Inheritance = Ex. a car is a vehicle
        //allows modeling objects inherit as a subset of another object. It defines "IS A relationship".
        //inheritance can be defined as the process where one class acquires the properties (method and fields) of another class.

        //extends - is used to inherit the properties of a class
        //super used for referencing the variable, properties or methods which can be used to another class.

        Dog dog1 = new Dog();
        System.out.println(dog1.getBreed());//composition

        //interfaces
        //is used to achieve the abstraction
        //In general terms, an interface can be defined as a container that stores the signature of methods to be implemented in the code segment

        Person jane = new Person();
        jane.run();
        jane.sleep();
        jane.holidayGreet();
        jane.morningGreet();

        //POLYMORPHISM - ability to take on many forms
        //there are 2 main types
        //1. Static or Compile Polymorphism
        // is usually done by function/method overloading

        StaticPoly print = new StaticPoly();
        System.out.println(print.addition(5,5));
        System.out.println(print.addition(5,5,5));
        System.out.println(print.addition(5.6,5.8));

        //2. Dynamic Method Dispatch or Runtime Polymorphism
        //is usually done by function/method overriding

        Parent parent1 = new Parent();
        parent1.speak();//method of superClass or parent class
        Parent subObj = new Child();
        subObj.speak();


    }

}
