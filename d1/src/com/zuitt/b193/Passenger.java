package com.zuitt.b193;

public class Passenger {

    private String passengerName;

    public Passenger(String passengerName){
        this.passengerName = passengerName;
    }

    public String getPassengerName(){
        return this.passengerName;
    }

}
