package com.zuitt.b193;

public class Main {

    public static void main(String[] args){

        //5.
       User user = new User("Tee Jae","Calinao", 25, "Antipolo City");
        //6.
       Course course =  new Course();
        //7.
       course.setName("Physics 101");
       course.setDescription("Learn Physics");
       course.setSeats(30);
       course.setFee(1500.00);
       course.setInstructor(user);

       //8.
        System.out.println("User's First Name: " + user.getFirstName() );
        System.out.println("User's Last Name: " + user.getLastName() );
        System.out.println("User's Age: " + user.getAge() );
        System.out.println("User's Address: " + user.getAddress() );

        //9.
        System.out.println("Course Name: " + course.getName());
        System.out.println("Course Description: " + course.getDescription());
        System.out.println("Course Fee: " + course.getFee());
        System.out.println("Course Instructor: " + course.getInstructor().getFirstName());

        course.printEnrollees();

        course.addEnrollee("Louise Simpelo");
        course.addEnrollee("Marvin Gomez");
        course.addEnrollee("Albert Magtibay");

        course.printEnrollees();

    }

}
